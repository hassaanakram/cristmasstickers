import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hp.cristmasstickers.R;
import com.example.hp.cristmasstickers.utils.Utils;

import yuku.ambilwarna.AmbilWarnaDialog;

import static android.app.Activity.RESULT_OK;

public class customDialog extends Dialog implements android.view.View.OnClickListener {
    public Activity c;
    public Dialog d;
    public Button pick, send;
    EditText getText;
    String text,colorToSend;

    public customDialog(Activity a) {
        super( a );
        // TODO Auto-generated constructor stub
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        requestWindowFeature( Window.FEATURE_NO_TITLE );
        setContentView( R.layout.text_dialog );
        pick = findViewById( R.id.pick_color );
        send = findViewById( R.id.send );
        getText = findViewById( R.id.get_text );
        pick.setOnClickListener( this );
        send.setOnClickListener( this );

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pick_color:
                AmbilWarnaDialog dialog=new AmbilWarnaDialog( c, Color.parseColor( "#000000" ), new AmbilWarnaDialog.OnAmbilWarnaListener() {
                    @Override
                    public void onCancel(AmbilWarnaDialog dialog) {

                    }

                    @Override
                    public void onOk(AmbilWarnaDialog dialog, int color) {

                    }
                } );
                dialog.show();
                break;

            case R.id.send:
//                if (Utils.isNotEmpty( getText.getText() )) {
//                    text = getText.getText().toString();
//                    if (Utils.isNotEmpty( colorToSend )) {
//                        Intent intent = new Intent();
//                        intent.putExtra( "text", text );
//                        intent.putExtra( "color", colorToSend );
//                        c.setResult( RESULT_OK, intent );
//                        c.finish();
//                    } else {
//                        Toast.makeText( this, "Please select a color", Toast.LENGTH_SHORT ).show();
//                    }
//                } else {
//                    Toast.makeText( this, "Please enter text", Toast.LENGTH_SHORT ).show();
//                }
                break;
            default:
                break;
        }
        dismiss();
    }
}

