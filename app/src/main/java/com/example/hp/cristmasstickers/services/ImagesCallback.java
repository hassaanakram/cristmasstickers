package com.example.hp.cristmasstickers.services;


import com.example.hp.cristmasstickers.models.ImagesModel;

public interface ImagesCallback {

    void onImagesListDownloaded(ImagesModel data);
    void onError(String error);
}
