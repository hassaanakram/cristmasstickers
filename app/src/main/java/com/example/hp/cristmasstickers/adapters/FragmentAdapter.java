package com.example.hp.cristmasstickers.adapters;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.hp.cristmasstickers.ChristmasBalloons;
import com.example.hp.cristmasstickers.ChristmasChurch;
import com.example.hp.cristmasstickers.ChristmasGiftsPacks;
import com.example.hp.cristmasstickers.ChristmasQuotes;
import com.example.hp.cristmasstickers.ChristmasSenta;
import com.example.hp.cristmasstickers.ChristmasStickers;
import com.example.hp.cristmasstickers.ChristmasTrees;

public class FragmentAdapter extends FragmentPagerAdapter {


    public FragmentAdapter(FragmentManager fm) {
        super( fm );
    }

    @Override
    public Fragment getItem(int i) {
        Bundle info1;
        info1 = new Bundle();
        switch (i) {
            case 0:
                ChristmasStickers stickers = new ChristmasStickers();
                info1.putInt( "CurrentPage", i++ );
                stickers.setArguments( info1 );
                return stickers;
            //return  new ChristmasBalloons();
            case 1:
                return new ChristmasSenta();
            case 2:
                return new ChristmasGiftsPacks();
            case 3:
                return new ChristmasTrees();
            case 4:
                ChristmasBalloons balloons = new ChristmasBalloons();
                info1.putInt( "CurrentPage", i++ );
                balloons.setArguments( info1 );
                return balloons;
            case 5:
                return new ChristmasQuotes();
            case 6:
                return new ChristmasChurch();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 7;
    }

    public CharSequence getPageTitle(int position) {
        switch (position) {

            case 0:
                return "Sticker";
            case 1:
                return "Santa";
            case 2:
                return "Gift";
            case 3:
                return "Tree";
            case 4:
                return "Balloon";
            case 5:
                return "Quote";
            case 6:
                return "Church";

        }
        return null;
    }
}


