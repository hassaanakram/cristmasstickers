package com.example.hp.cristmasstickers.adapters;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class PicturesAdapter extends BaseAdapter {

    private Activity ctx;
    private final String[] filesNames;
    private final String[] filesPaths;

    public PicturesAdapter(Activity ctx, String[] filesNames, String[] filesPaths) {
        this.ctx = ctx;
        this.filesNames = filesNames;
        this.filesPaths = filesPaths;
    }

    @Override
    public int getCount() {
        return filesPaths.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ImageView imageView;
        if (convertView == null) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ctx.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;
            // if it's not recycled, initialize some attributes
            imageView = new ImageView( ctx );
            imageView.setLayoutParams( new AbsListView.LayoutParams( width/4, width/4 ) );
            imageView.setScaleType( ImageView.ScaleType.CENTER_CROP );
            imageView.setPadding( 8, 8, 18, 8 );
            Bitmap bmp = BitmapFactory.decodeFile( filesPaths[position] );
            if (bmp != null) {
//            imageView.setImageBitmap(bmp);
                Glide.with( ctx ).load( bmp ).into( imageView );
            }
        } else {
            imageView = (ImageView) convertView;
        }

        return imageView;
    }
}
