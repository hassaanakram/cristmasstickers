package com.example.hp.cristmasstickers.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.example.hp.cristmasstickers.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;


public class AdUtils {


    public static void requestNewInterstitial(Activity activity, InterstitialAd interstitialAd) {
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("B2F3A0EBC92D1DC41EA3C185955838E3").build();
        if (interstitialAd.getAdUnitId() == null)
            interstitialAd.setAdUnitId(activity.getString(R.string.interstitial_ad_id));
        if (new BillingHelper().shouldShowAds(activity))
            interstitialAd.loadAd(adRequest);
    }

    public static void showAdAndMoveForward(final Context context, final InterstitialAd interstitialAd, final Intent intent) {
        if (interstitialAd != null && interstitialAd.isLoaded()) { //agr gogole ne ad bj dya ha show ho jaega
            interstitialAd.show();
        } else {                         // aur agr ni bja to next page show ho jae

            context.startActivity(intent);
        }
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {       //ad close krne pr next page show kr dega
                context.startActivity(intent);
            }
        });
    }

    public static void showAdAndMoveForwardForResult(final Activity context, final InterstitialAd interstitialAd, final Intent intent, final int requestCode) {
        if (interstitialAd != null && interstitialAd.isLoaded()) { //agr gogole ne ad bj dya ha show ho jaega
            interstitialAd.show();

        } else {                         // aur agr ni bja to next page show ho jae

            context.startActivityForResult(intent, requestCode);
        }
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {       //ad close krne pr next page show kr dega
                context.startActivityForResult(intent, requestCode);
            }
        });
    }

    public static void loadBannerAd(Activity activity) {
        final AdView adView = activity.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("B2F3A0EBC92D1DC41EA3C185955838E3").build();

        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                adView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }
        });
        if (new BillingHelper().shouldShowAds(activity))
            adView.loadAd(adRequest);
    }

    public static void backPressLoadAd(final Activity context, InterstitialAd interstitialAd) {
        if (interstitialAd.isLoaded()) {
            interstitialAd.show();
        } else {
            context.finish();
        }
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                context.finish();
            }
        });
    }


}
