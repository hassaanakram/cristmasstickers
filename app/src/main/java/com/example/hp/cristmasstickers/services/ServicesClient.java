package com.example.hp.cristmasstickers.services;

import android.support.annotation.NonNull;
import com.example.hp.cristmasstickers.models.ImagesModel;
import com.google.gson.Gson;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServicesClient {

    public ServicesClient() {
    }

    public void getAllImages(final ImagesCallback callback, String id) {

        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put("method", "flickr.photosets.getPhotos");
        queryMap.put("api_key", Constants.API_KEY);
        queryMap.put("photoset_id", id);
        queryMap.put("user_id", Constants.USER_ID);
        queryMap.put("format", "json");
        queryMap.put("nojsoncallback", "1");

        RetrofitClient.getClient().create(ApiClient.class).getAllImages(queryMap).enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                ImagesModel data = new Gson().fromJson(response.body(), ImagesModel.class);
                if (response.isSuccessful()) {
                    callback.onImagesListDownloaded(data);
                } else {
                    callback.onError("Something went wrong...");
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                if (t.getMessage().isEmpty()) {
                    callback.onError("Something went wrong...");
                } else {
                    callback.onError(t.getMessage());
                }
            }
        });
    }
}
