package com.example.hp.cristmasstickers.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;


import java.io.File;
import java.io.FileOutputStream;

public class ImageHelper {



    public static void downloadImage(final Context context, String url, final String photoId) {
        Glide.with(context)
                .asBitmap()
                .load(url)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, Transition<? super Bitmap> transition) {
                        saveToLocalStorage(context, photoId, resource);
                    }
                });
    }

    public static void saveToLocalStorage(Context context, String photoId, Bitmap bitmap) {

        //save to local storage
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/Christmas Stickers");
        if (!myDir.exists())
            myDir.mkdirs();
        String fileName = photoId + ".jpg";
        File file = new File(myDir, fileName);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            Toast.makeText(context, "Image Saved...", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
