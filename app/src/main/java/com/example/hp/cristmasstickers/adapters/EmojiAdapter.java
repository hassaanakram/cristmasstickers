package com.example.hp.cristmasstickers.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.hp.cristmasstickers.R;

import java.util.ArrayList;

public class EmojiAdapter extends BaseAdapter {
    private ArrayList<String> images;
    private Context context;
    LayoutInflater layoutInflater;

    public EmojiAdapter(Context context, ArrayList<String> images) {
        this.context = context;
        this.images = images;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView;
        if (convertView == null) {

            convertView = layoutInflater.from( context ).inflate( R.layout.emoji_adapter, parent, false );
            textView = convertView.findViewById( R.id.emoji_text );

        } else {
            textView = (TextView) convertView;
        }

        textView.setText( images.get( position ) );
        return textView;
    }

}
