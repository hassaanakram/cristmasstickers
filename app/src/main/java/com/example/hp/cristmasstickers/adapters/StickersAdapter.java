package com.example.hp.cristmasstickers.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.hp.cristmasstickers.R;

public class StickersAdapter extends BaseAdapter {
    private Integer[] images;
    private Context context;
    LayoutInflater layoutInflater;

    public StickersAdapter(Context context,Integer[] images ){
        this.context=context;
        this.images=images;
    }
    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {

            convertView = layoutInflater.from(context).inflate(R.layout.stikers_adapter,parent,false);
            imageView= convertView.findViewById(R.id.imageView);

        } else {
            imageView = (ImageView) convertView;
        }
//        imageView.setImageResource(images[position]);
        Glide.with( context ).load( images[position] ).into( imageView );
        return imageView;
    }

}
