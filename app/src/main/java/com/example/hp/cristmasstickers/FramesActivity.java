package com.example.hp.cristmasstickers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import android.support.v7.app.AppCompatActivity;

import com.example.hp.cristmasstickers.ui.OverlayActivity;

public class FramesActivity extends AppCompatActivity {
    MyAdapter myAdapter;
    ViewPager viewPager;
    Toolbar toolbar;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frames);

        toolbar = findViewById(R.id.layout_title_bar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Swipe For Next, Click To Select");
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_circle_left);
        }
        Window window = this.getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.black));
        }
        int[] frames = {R.drawable.frametoshowa, R.drawable.frametoshowb, R.drawable.frametoshowc,
                R.drawable.frametoshowe,
                R.drawable.frametoshowg, R.drawable.frametoshowh, R.drawable.frametoshowi, R.drawable.frametoshowx, R.drawable.frametoshowy, R.drawable.frametoshowz};

        viewPager = findViewById(R.id.viewpager);
        myAdapter = new MyAdapter(this, frames);
        viewPager.setAdapter(myAdapter);
    }

    class MyAdapter extends PagerAdapter {
        Context context;
        int imageArray[];

        public MyAdapter(Context context, int[] imgArra) {
            imageArray = imgArra;
            this.context = context;
        }

        @Override
        public int getCount() {
            return imageArray.length;
        }


        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
            return view == (o);
        }


        @Override
        public Object instantiateItem(ViewGroup container, final int position) {

            final View itemView = LayoutInflater.from(context).inflate(R.layout.item_view, container, false);

            final ImageView imageView = itemView.findViewById(R.id.image_view);
            Glide.with(context)
                    .asBitmap()
                    .load(imageArray[position])
                    .into(imageView);

            container.addView(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, OverlayActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra("id", position);
                    startActivity(intent);
                    finish();
                }
            });
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((RelativeLayout) object);
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}


