package com.example.hp.cristmasstickers.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.example.hp.cristmasstickers.BuildConfig;
import com.example.hp.cristmasstickers.FramesActivity;
import com.example.hp.cristmasstickers.R;
import com.example.hp.cristmasstickers.SavedPictures;
import com.example.hp.cristmasstickers.utils.AdUtils;
import com.example.hp.cristmasstickers.utils.BillingHelper;
import com.example.hp.cristmasstickers.utils.Constants;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, BillingProcessor.IBillingHandler {

    public static final int PERMISSION_REQUEST_CODE = 2, PICK_FROM_GALLERY = 3;
    static final int REQUEST_TAKE_PHOTO = 4;


    int counter = 0;
    ImageView camera, gallery, saved, star, shareIv;
    WebView view;
    Context context;
    private String mCurrentPhotoPath = "jyfthf";
    Uri photoURI;
    TextView galleryTv, camTv, frameTv, savedphotoTv, shareTv, rateTv;
    ImageView frameSelectionIv;
    RotateAnimation animationRotate;
    InterstitialAd interstitialAd;
    private Button removeAdsBtn;
    private BillingHelper billingHelper;
    private BillingProcessor bp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        billingHelper = new BillingHelper();
        bp = new BillingProcessor(this, getString(R.string.in_app_billing_key), this);
        bp.initialize();
        interstitialAd = new InterstitialAd(this);
        frameSelectionIv = findViewById(R.id.framing);
        frameSelectionIv.setOnClickListener(this);
        camera = findViewById(R.id.camera);
        camera.setOnClickListener(this);
        gallery = findViewById(R.id.gallery);
        gallery.setOnClickListener(this);
        saved = findViewById(R.id.save);
        saved.setOnClickListener(this);
        star = findViewById(R.id.star);
        star.setOnClickListener(this);
        shareIv = findViewById(R.id.share_iv);
        shareIv.setOnClickListener(this);
        galleryTv = findViewById(R.id.gallry_tv);
        galleryTv.setOnClickListener(this);
        camTv = findViewById(R.id.camera_tv);
        camTv.setOnClickListener(this);
        shareTv = findViewById(R.id.share_tv);
        shareTv.setOnClickListener(this);
        rateTv = findViewById(R.id.star_tv);
        rateTv.setOnClickListener(this);
        savedphotoTv = findViewById(R.id.save_tv);
        savedphotoTv.setOnClickListener(this);
        frameTv = findViewById(R.id.framing_tv);
        frameTv.setOnClickListener(this);
        removeAdsBtn = findViewById(R.id.removeAdsBtn);
        if (billingHelper.shouldShowAds(this)){
            removeAdsBtn.setVisibility(View.VISIBLE);
            removeAdsBtn.setOnClickListener(this);
            removeAdsBtn.setEnabled(true);
        }

        @SuppressLint("ResourceType") Animation anim = AnimationUtils.loadAnimation(this, R.animator.anim);
        shareIv.startAnimation(anim);
        try {
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]
                        {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
            }
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.CAMERA},
                        PERMISSION_REQUEST_CODE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        animationRotate = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

        animationRotate.setInterpolator(new LinearInterpolator());
        animationRotate.setRepeatCount(Animation.INFINITE); //Repeat animation indefinitely
        animationRotate.setDuration(1800); //Put desired duration per anim cycle here, in milliseconds

        star.startAnimation(animationRotate);


    }

    @Override
    protected void onResume() {
        super.onResume();
        AdUtils.requestNewInterstitial(this, interstitialAd);
        AdUtils.loadBannerAd(this);
    }

    private Intent rateIntentForUrl(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("%s?id=%s", url, getPackageName())));
        int flags = Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK;
        if (Build.VERSION.SDK_INT >= 21) {
            flags |= Intent.FLAG_ACTIVITY_NEW_DOCUMENT;
        } else {
            //noinspection deprecation
            flags |= Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET;
        }
        intent.addFlags(flags);
        return intent;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(MainActivity.this,
                        "com.example.hp.cristmasstickers",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                //       photoURI=null;
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);

            }
        }
    }

    private boolean cameraCheckPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    private boolean galleryCheckPermission() {
        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            return false;

        }
        return true;

    }

    private void cameraRequestPermission() {
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.CAMERA},
                PERMISSION_REQUEST_CODE);
    }

    private void galleryRequestPermission() {
        ActivityCompat.requestPermissions(MainActivity.this, new String[]
                        {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                PICK_FROM_GALLERY);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Snackbar.make(findViewById(android.R.id.content), "You will have to allow permissions to go to next screen", Snackbar.LENGTH_LONG)
                            .setAction("Allow", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    cameraRequestPermission();
                                }
                            })
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
//
                }
                break;
            case PICK_FROM_GALLERY:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Snackbar.make(findViewById(android.R.id.content), "You will have to allow permissions to go to next screen", Snackbar.LENGTH_LONG)
                            .setAction("Allow", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    galleryRequestPermission();
                                }
                            })
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
//
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        String root = getExternalFilesDir(Environment.DIRECTORY_DCIM).getAbsolutePath() + "/Christmas Wallpapers";
        File file = new File(root);
        if (!file.exists())
            file.mkdirs();
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                file      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {

                Intent intent = new Intent(MainActivity.this, DisplayActivity.class);
                intent.putExtra("CameraImage", mCurrentPhotoPath);
                startActivity(intent);

            } else if (requestCode == PICK_FROM_GALLERY && data != null) {
                final Uri imageUri = data.getData();
                Intent intentForGallery = new Intent(MainActivity.this, DisplayActivity.class);
                intentForGallery.putExtra("BitmapImageGallery", imageUri.toString());
                startActivity(intentForGallery);

            }
        }
    }

    private void invokeRateUsIntent() {
        Uri uri = Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID);
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)));
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.share_iv:
            case R.id.share_tv:
                onClickAfterAd(v.getId());
                break;
            case R.id.star:
            case R.id.star_tv:
                onClickAfterAd(v.getId());
                break;
            case R.id.removeAdsBtn:
                bp.purchase(this, Constants.PRODUCT_KEY);
                break;
            default:
                    showAd(v.getId());

        }
    }

    private void showAd(final int id) {
        if (interstitialAd.isLoaded()) {
            interstitialAd.show();
            interstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    AdUtils.requestNewInterstitial(MainActivity.this, interstitialAd);
                    onClickAfterAd(id);
                }

                @Override
                public void onAdClicked() {
                    super.onAdClicked();
                    AdUtils.requestNewInterstitial(MainActivity.this, interstitialAd);
                    onClickAfterAd(id);
                }
            });
        }else{
            onClickAfterAd(id);
        }
    }

    private void onClickAfterAd(int id) {
        switch (id) {
            case R.id.framing:
            case R.id.framing_tv:
                Intent intentTv = new Intent(MainActivity.this, FramesActivity.class);
                startActivity(intentTv);
                break;
            case R.id.share_iv:
            case R.id.share_tv:
                Intent sendIntentTv = new Intent();
                sendIntentTv.setAction(Intent.ACTION_SEND);
                sendIntentTv.putExtra(Intent.EXTRA_TEXT,
                        "Hey check out this new app at: https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
                sendIntentTv.setType("text/plain");
                startActivity(sendIntentTv);
                break;
            case R.id.camera:
            case R.id.camera_tv:
                if (cameraCheckPermission()) {
                    dispatchTakePictureIntent();

                } else {
                    cameraRequestPermission();
                }

                break;
            case R.id.star:
            case R.id.star_tv:
                invokeRateUsIntent();
                break;
            case R.id.gallery:
            case R.id.gallry_tv:
                if (galleryCheckPermission()) {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
                } else {
                    galleryRequestPermission();
                }
                break;
            case R.id.save:
            case R.id.save_tv:
                Intent intent1 = new Intent(MainActivity.this, SavedPictures.class);
                startActivity(intent1);
                break;
        }
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
        Toast.makeText(context, "Restart app to remove ads", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {

    }

    @Override
    public void onBillingInitialized() {

    }
}
