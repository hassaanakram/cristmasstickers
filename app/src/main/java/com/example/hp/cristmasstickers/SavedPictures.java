package com.example.hp.cristmasstickers;

import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.example.hp.cristmasstickers.adapters.PicturesAdapter;
import com.example.hp.cristmasstickers.ui.FullImageActivity;
import com.example.hp.cristmasstickers.ui.MainActivity;
import com.example.hp.cristmasstickers.utils.AdUtils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;

import java.io.File;

public class SavedPictures extends AppCompatActivity {

    GridView gridView;
    private File[] files;
    int counter;
    private String[] filesPaths;
    private String[] filesNames;
    InterstitialAd interstitialAd;
    TextView noImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_pictures);
        gridView = findViewById(R.id.gridview);

        noImage=findViewById(R.id.noimage_tv);
        interstitialAd = new InterstitialAd(this);
        AdUtils.requestNewInterstitial(this, interstitialAd);
        AdUtils.loadBannerAd(this);

        Toolbar toolbar = findViewById(R.id.layout_title_bar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Saved Images");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_circle_left);
        }
        File dirDownload = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (dirDownload.isDirectory()) {
            files = dirDownload.listFiles();
            filesPaths = new String[files.length];
            filesNames = new String[files.length];

            for (int i = 0; i < files.length; i++) {
                noImage.setVisibility(View.GONE);
                if (files[i].getName().toLowerCase().endsWith("jpg") || files[i].getName().toLowerCase().endsWith("png") || files[i].getName().toLowerCase().endsWith("jpeg")) {
                    filesPaths[i] = files[i].getAbsolutePath();
                    filesNames[i] = files[i].getName();
                }
            }
        }
        gridView.setAdapter(new PicturesAdapter(this, filesNames, filesPaths));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                counter++;
                if (counter % 8 == 0 && interstitialAd.isLoaded()) {
                    showAd();
                }
                Intent intent = new Intent(SavedPictures.this, FullImageActivity.class);
                intent.putExtra("img", filesPaths[position]);
                intent.putExtra("nam", filesNames[position]);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showAd() {
        interstitialAd.show();
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                AdUtils.requestNewInterstitial(SavedPictures.this, interstitialAd);
            }

            @Override
            public void onAdClicked() {
                super.onAdClicked();
                AdUtils.requestNewInterstitial(SavedPictures.this, interstitialAd);
            }
        });
    }
}
