package com.example.hp.cristmasstickers.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.hp.cristmasstickers.R;
import com.example.hp.cristmasstickers.utils.AdUtils;
import com.google.android.gms.ads.InterstitialAd;

import static com.example.hp.cristmasstickers.ui.MainActivity.PERMISSION_REQUEST_CODE;
import static com.example.hp.cristmasstickers.ui.MainActivity.PICK_FROM_GALLERY;

public class SplashActivity extends AppCompatActivity {

    InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        interstitialAd = new InterstitialAd(this);

        if (!hasAllPermissions()) {

            requestPermissions();
        }else {
            AdUtils.requestNewInterstitial(this, interstitialAd);
            int SPLASH_DISPLAY_LENGTH = 3000;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    AdUtils.showAdAndMoveForward(SplashActivity.this, interstitialAd, i);
                    finish();
                }
            }, SPLASH_DISPLAY_LENGTH);
        }


    }

    private boolean hasAllPermissions() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        String[] permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        ActivityCompat.requestPermissions(this, permissions, 0);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        AdUtils.requestNewInterstitial(this, interstitialAd);
        int SPLASH_DISPLAY_LENGTH = 2500;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                AdUtils.showAdAndMoveForward(SplashActivity.this, interstitialAd, i);
                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
