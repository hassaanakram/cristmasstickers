package com.example.hp.cristmasstickers.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.example.hp.cristmasstickers.BuildConfig;
import com.example.hp.cristmasstickers.R;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ja.burhanrashid52.photoeditor.PhotoEditor;
import ja.burhanrashid52.photoeditor.PhotoEditorView;
import yuku.ambilwarna.AmbilWarnaDialog;

public class BackgroundFramesActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView camerImage;
    ImageView galleryImage;
    ImageView setColor;
    PhotoEditor mPhotoEditor;
    PhotoEditorView photoEditorView;
    public static final int PERMISSION_REQUEST_CODE = 2, PICK_FROM_GALLERY = 3;
    static final int REQUEST_TAKE_PHOTO = 4;
    private String mCurrentPhotoPath = "jyfthf", frameFilePath;
    Uri photoURI;
    View root;
    Bitmap largeIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_background_frames);

        camerImage = findViewById(R.id.camera_image);
        galleryImage = findViewById(R.id.gallery_image);
        setColor = findViewById(R.id.select_frame_color);
        FloatingActionButton frame_fab = findViewById(R.id.save_frame_to_directory);
        camerImage.setOnClickListener(this);
        galleryImage.setOnClickListener(this);
        setColor.setOnClickListener(this);
        frame_fab.setOnClickListener(this);
        View someView = findViewById(R.id.someview);
        root = someView.getRootView();

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().hide();

        final Typeface mTextRobotoTf = ResourcesCompat.getFont(this, R.font.myfile);

        photoEditorView = findViewById(R.id.photoEditorViewFrames);
//        final SubsamplingScaleImageView imageView = findViewById(R.id.imageView);
//        imageView.setImage(ImageSource.resource(R.drawable.frametoshowa));
        mPhotoEditor = new PhotoEditor.Builder(this, photoEditorView)
                .setPinchTextScalable(true)
                .setDefaultTextTypeface(mTextRobotoTf)
                .build();
//        photoEditorView.getSource().setImageBitmap(icon);
        final Intent intent = getIntent();
        if (getIntent().hasExtra("id")) {
            int i = intent.getIntExtra("id", 1);

            Drawable drawable;
            switch (i) {
                case 0:
//                    drawable = ContextCompat.getDrawable(this, R.drawable.frametoshowa);
//                    photoEditorView.getSource().setImageDrawable(drawable);
                    largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.frametoshowa);
                    photoEditorView.getSource().setImageBitmap(largeIcon);

                    break;
                case 1:
                    drawable = ContextCompat.getDrawable(this, R.drawable.frametoshowb);
                    photoEditorView.getSource().setImageDrawable(drawable);
                    largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.frametoshowb);
                    photoEditorView.getSource().setImageBitmap(largeIcon);
                    break;
                case 2:

                    drawable = ContextCompat.getDrawable(this, R.drawable.frametoshowc);
                    photoEditorView.getSource().setImageDrawable(drawable);
                    largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.frametoshowc);
                    photoEditorView.getSource().setImageBitmap(largeIcon);
                    break;
//                case 3:
//
//
//                    drawable = ContextCompat.getDrawable(this, R.drawable.frametoshowd);
//                    photoEditorView.getSource().setImageDrawable(drawable);
//                    largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.frametoshowd);
//                    photoEditorView.getSource().setImageBitmap(largeIcon);
//                    break;
                case 4:

                    drawable = ContextCompat.getDrawable(this, R.drawable.frametoshowe);
                    photoEditorView.getSource().setImageDrawable(drawable);
                    largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.frametoshowe);
                    photoEditorView.getSource().setImageBitmap(largeIcon);
                    break;

                case 6:


                    drawable = ContextCompat.getDrawable(this, R.drawable.frametoshowg);
                    photoEditorView.getSource().setImageDrawable(drawable);
                    largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.frametoshowg);
                    photoEditorView.getSource().setImageBitmap(largeIcon);
                    break;
                case 7:


                    drawable = ContextCompat.getDrawable(this, R.drawable.frametoshowh);
                    photoEditorView.getSource().setImageDrawable(drawable);
                    largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.frametoshowh);
                    photoEditorView.getSource().setImageBitmap(largeIcon);
                    break;
                case 8:

                    drawable = ContextCompat.getDrawable(this, R.drawable.frametoshowi);
                    photoEditorView.getSource().setImageDrawable(drawable);
                    largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.frametoshowi);
                    photoEditorView.getSource().setImageBitmap(largeIcon);
                    break;


            }


        }

    }

    private boolean cameraCheckPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    private boolean galleryCheckPermission() {
        if (ActivityCompat.checkSelfPermission(BackgroundFramesActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            return false;

        }
        return true;

    }

    private void cameraRequestPermission() {

        ActivityCompat.requestPermissions(BackgroundFramesActivity.this,
                new String[]{Manifest.permission.CAMERA},
                PERMISSION_REQUEST_CODE);
    }

    private void galleryRequestPermission() {
        ActivityCompat.requestPermissions(BackgroundFramesActivity.this, new String[]
                        {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                PICK_FROM_GALLERY);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.camera_image:
                if (cameraCheckPermission()) {
                    dispatchTakePictureIntent();

                } else {
                    cameraRequestPermission();
                }
                break;
            case R.id.gallery_image:
                if (galleryCheckPermission()) {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
                } else {
                    galleryRequestPermission();
                }
                break;
            case R.id.save_frame_to_directory:
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(BackgroundFramesActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(BackgroundFramesActivity.this);
                }

                builder.setTitle("Save Image")
                        .setMessage("Are you sure you want to save this Image?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with save
                                if (ActivityCompat.checkSelfPermission(BackgroundFramesActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                                    ActivityCompat.requestPermissions(BackgroundFramesActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                                    return;
                                }
                                try {
                                    frameFilePath = createFrameImagePath();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                mPhotoEditor.saveAsFile(frameFilePath, new PhotoEditor.OnSaveListener() {
                                    @Override
                                    public void onSuccess(@NonNull String imagePath) {
                                        Toast.makeText(BackgroundFramesActivity.this, "saved", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(BackgroundFramesActivity.this, MainActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }

                                    @Override
                                    public void onFailure(@NonNull Exception exception) {
                                        Toast.makeText(BackgroundFramesActivity.this, "failure", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }

                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        }).setIcon(R.drawable.icon)
                        .show();


                break;
            case R.id.select_frame_color:
                AmbilWarnaDialog dialog = new AmbilWarnaDialog(BackgroundFramesActivity.this, Color.parseColor("#000000"),
                        new AmbilWarnaDialog.OnAmbilWarnaListener() {
                            @Override
                            public void onCancel(AmbilWarnaDialog dialog) {

                            }

                            @SuppressLint("ResourceType")
                            @Override
                            public void onOk(AmbilWarnaDialog dialog, int color) {
                                //   colorToSend = String.valueOf(color);
                                root.setBackgroundColor(color);

                                photoEditorView.setBackgroundColor(color);

                            }
                        });
                dialog.show();
        }

    }


    private String createFrameImagePath() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        frameFilePath = image.getAbsolutePath();
        return frameFilePath;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(BackgroundFramesActivity.this,
                        "com.example.hp.crismasstickers",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                //       photoURI=null;
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);

            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {

                // final Uri cameraImageuri= Uri.parse(String.valueOf(photoURI));

                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), photoURI);
                    Bitmap original = BitmapFactory.decodeStream(getAssets().open("1024x768.jpg"));
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    original.compress(Bitmap.CompressFormat.PNG, 100, out);
                    Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
                    photoEditorView.getSource().setImageBitmap(decoded);
                    if (largeIcon != null)
                        mPhotoEditor.addImage(largeIcon);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                photoEditorView.getSource().setImageBitmap(bitmap);
                if (largeIcon != null)
                    mPhotoEditor.addImage(largeIcon);

            } else if (requestCode == PICK_FROM_GALLERY && data != null) {
                final Uri galleryImageUri = data.getData();

                try {

                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), galleryImageUri);
                    photoEditorView.getSource().setImageBitmap(bitmap);
                    if (largeIcon != null)
                        mPhotoEditor.addImage(largeIcon);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        }
    }

}
