package com.example.hp.cristmasstickers.services;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface ApiClient {

    @GET("rest")
    Call<String> getAllImages(@QueryMap HashMap<String, String> queryMap);
}
