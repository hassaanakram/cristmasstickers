package com.example.hp.cristmasstickers.services;


import com.example.hp.cristmasstickers.models.Photo;

public class Constants {

    public static final String DATA_PASS_KEY = "DATA_PASS_KEY";
    public static final String BASE_URL = "https://api.flickr.com/services/";
    public static final String API_KEY = "592c7aaf03e74a387b53ac887b907c4e";
    public static final String APP_SECRET = "40d6a51cbb4a9092";
    public static final String USER_ID = "145090286@N02";
    public static final String ALL_PHOTO_SET_ID = "72157699248373682";

    public static String getStaticUrl(Photo photo){
        String url = "https://farm_farmid_.staticflickr.com/_server-id_/_id___secret_.jpg";
        url =  url.replace("_farmid_", String.valueOf(photo.getFarm()));
        url =  url.replace("_server-id_", String.valueOf(photo.getServer()));
        url =  url.replace("_id_", String.valueOf(photo.getId()));
        url =  url.replace("_secret_", String.valueOf(photo.getSecret()));
        return url;
    }

}
