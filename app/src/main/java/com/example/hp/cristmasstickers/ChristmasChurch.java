package com.example.hp.cristmasstickers;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.hp.cristmasstickers.adapters.StickersAdapter;

public class ChristmasChurch extends Fragment {

    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach( context );
        this.context = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (context == null) context = getActivity();
    }


    private Integer[] churchs={ R.drawable.ca,R.drawable.cb,R.drawable.cc,R.drawable.cd,
            R.drawable.ce,R.drawable.cf,R.drawable.cg,R.drawable.ch,R.drawable.ci,R.drawable.cj};

    GridView gridView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate( R.layout.christmas_stickers, null );
        gridView = (GridView) rootView.findViewById( R.id.gridview );
        gridView.setAdapter( new StickersAdapter( getContext(), churchs ) );
        gridView.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                ((SelectIcon) context).getSticker( churchs[position] );

            }
        } );
        return rootView;
    }
}
