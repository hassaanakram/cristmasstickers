package com.example.hp.cristmasstickers;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.hp.cristmasstickers.adapters.StickersAdapter;

public class ChristmasStickers extends Fragment {

    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach( context );
        this.context = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (context == null) context = getActivity();
    }

    private Integer[] stickers={ R.drawable.sa,R.drawable.sb,R.drawable.sc,R.drawable.sd,R.drawable.se,
            R.drawable.sf,R.drawable.sg,R.drawable.sh,R.drawable.so,
            R.drawable.sk,R.drawable.sl,R.drawable.sm,R.drawable.sn,
            R.drawable.sq,R.drawable.st,R.drawable.sz,R.drawable.sp,
            R.drawable.su,R.drawable.sv,R.drawable.sw,R.drawable.sx,R.drawable.sy,
            R.drawable.saa,R.drawable.sab,R.drawable.sac};
    GridView gridView;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate( R.layout.christmas_stickers, null );
        gridView = (GridView) rootView.findViewById( R.id.gridview );
        gridView.setAdapter( new StickersAdapter( getContext(), stickers ) );
        gridView.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                ((SelectIcon) context).getSticker( stickers[position] );

            }
        } );
        return rootView;
    }

}
