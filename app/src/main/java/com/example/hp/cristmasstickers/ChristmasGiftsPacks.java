package com.example.hp.cristmasstickers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.hp.cristmasstickers.adapters.StickersAdapter;

public class ChristmasGiftsPacks extends Fragment {

    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (context == null) context = getActivity();
    }

    private Integer[] gifts = {R.drawable.ga, R.drawable.gaa, R.drawable.gb, R.drawable.gab, R.drawable.gc,
            R.drawable.gac, R.drawable.gd, R.drawable.gad, R.drawable.ge, R.drawable.gae,
            R.drawable.gf, R.drawable.gaf, R.drawable.gg, R.drawable.gag, R.drawable.gh,
            R.drawable.gah, R.drawable.gi, R.drawable.gai, R.drawable.gj, R.drawable.gaj,
            R.drawable.gk, R.drawable.gak, R.drawable.gl, R.drawable.gal, R.drawable.gm,
            R.drawable.gam, R.drawable.gn, R.drawable.gan, R.drawable.go, R.drawable.gao,
            R.drawable.gp, R.drawable.gq, R.drawable.gr, R.drawable.gs, R.drawable.gt,
            R.drawable.gu, R.drawable.gv, R.drawable.gw, R.drawable.gx, R.drawable.gy,
            R.drawable.gz};
    GridView gridView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        @SuppressLint("InflateParams") View rootView = inflater.inflate(R.layout.christmas_stickers, null);
        gridView =rootView.findViewById(R.id.gridview);
        gridView.setAdapter(new StickersAdapter(getContext(), gifts));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                ((SelectIcon) context).getSticker(gifts[position]);

            }
        });
        return rootView;
    }
}
