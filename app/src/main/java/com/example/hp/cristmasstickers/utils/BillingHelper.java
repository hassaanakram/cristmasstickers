package com.example.hp.cristmasstickers.utils;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.example.hp.cristmasstickers.R;

public class BillingHelper implements BillingProcessor.IBillingHandler {

    private BillingCallback callback;

    public boolean shouldShowAds(Context context) {
        return !(new BillingProcessor(context, context.getString(R.string.in_app_billing_key), this).isPurchased(Constants.PRODUCT_KEY));
    }

    public void purchase(Activity context, BillingCallback callback) {
        BillingProcessor bp = new BillingProcessor(context, context.getString(R.string.in_app_billing_key), this);
        bp.purchase(context, Constants.PRODUCT_KEY);
        this.callback = callback;
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
        if (callback != null) callback.onPurchased();
    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {

    }

    @Override
    public void onBillingInitialized() {

    }

    public interface BillingCallback {
        void onPurchased();
    }
}
