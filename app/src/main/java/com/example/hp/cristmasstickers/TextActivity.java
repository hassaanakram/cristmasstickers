package com.example.hp.cristmasstickers;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hp.cristmasstickers.utils.Utils;

import yuku.ambilwarna.AmbilWarnaDialog;

public class TextActivity extends AppCompatActivity implements View.OnClickListener {

    EditText getText;
    String text;
    Button send, pickColor;
    String colorToSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_text );
        getText = findViewById( R.id.get_text );
        send = findViewById( R.id.send );
        send.setOnClickListener( this );
        pickColor = findViewById( R.id.pick_color );
        pickColor.setOnClickListener( this );
        text = getText.getText().toString();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.send:
                if (Utils.isNotEmpty( getText.getText() )) {
                    text = getText.getText().toString();
                    if (Utils.isNotEmpty( colorToSend )) {
                        Intent intent = new Intent();
                        intent.putExtra( "text", text );
                        intent.putExtra( "color", colorToSend );
                        setResult( RESULT_OK, intent );
                        finish();
                    } else {
                        Toast.makeText( this, "Please select a color", Toast.LENGTH_SHORT ).show();
                    }
                } else {
                    Toast.makeText( this, "Please enter text", Toast.LENGTH_SHORT ).show();
                }
                break;
            case R.id.pick_color:
                AmbilWarnaDialog dialog = new AmbilWarnaDialog( this, Color.parseColor( "#000000" ), new AmbilWarnaDialog.OnAmbilWarnaListener() {
                    @Override
                    public void onCancel(AmbilWarnaDialog dialog) {

                    }

                    @Override
                    public void onOk(AmbilWarnaDialog dialog, int color) {
                        if (text != null)
                            colorToSend = String.valueOf( color );
                        getText.setTextColor( color );


                    }
                } );
                dialog.show();

                break;
        }
    }
}
