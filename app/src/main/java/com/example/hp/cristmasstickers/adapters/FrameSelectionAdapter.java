package com.example.hp.cristmasstickers.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

public class FrameSelectionAdapter extends PagerAdapter {
    Activity activity;
    int imageArray[];
    public FrameSelectionAdapter(Activity act, int[] imgArra) {
        imageArray = imgArra;
        activity = act;
    }
    @Override
    public int getCount() {
        return imageArray.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return false;
    }
    @Override
    public void destroyItem(View arg0, int arg1, Object arg2) {
        ((ViewPager) arg0).removeView((View) arg2);
    }

}
