package com.example.hp.cristmasstickers.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class RetrofitClient {

    private static Retrofit retrofitClient = null;
    private static final int TIME_OUT = 30;

    public static Retrofit getClient() {
        if (retrofitClient == null) {
            OkHttpClient.Builder okClient = new OkHttpClient.Builder();
            okClient.connectTimeout(TIME_OUT, TimeUnit.SECONDS);
            okClient.readTimeout(TIME_OUT,TimeUnit.SECONDS);
            okClient.writeTimeout(TIME_OUT,TimeUnit.SECONDS);

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            okClient.addInterceptor(interceptor);

            Gson gson = new GsonBuilder().setLenient().create();

            retrofitClient = new Retrofit.Builder().baseUrl(Constants.BASE_URL).addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson)).client(okClient.build()).build();
        }
        return retrofitClient;
    }
}
