package com.example.hp.cristmasstickers.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import com.example.hp.cristmasstickers.R;
import com.example.hp.cristmasstickers.adapters.EmojiAdapter;
import java.util.ArrayList;
import ja.burhanrashid52.photoeditor.PhotoEditor;

public class EmojiActivity extends AppCompatActivity {

    GridView emojiList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_emoji );
        emojiList = findViewById( R.id.gridview );

        Toolbar toolbar = findViewById(R.id.layout_title_bar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null) {
            getSupportActionBar().setTitle("Click to Select Frame");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_circle_left);
        }

        final ArrayList<String> emojiArray = PhotoEditor.getEmojis( this );
        EmojiAdapter adapter=new EmojiAdapter( this,emojiArray );

        emojiList.setAdapter( adapter );
        emojiList.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.putExtra( "emoji", emojiArray.get( position ) );
                setResult( RESULT_OK, intent );
                finish();
            }
        } );

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
