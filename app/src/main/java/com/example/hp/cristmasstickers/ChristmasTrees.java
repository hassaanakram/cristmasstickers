package com.example.hp.cristmasstickers;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.hp.cristmasstickers.adapters.StickersAdapter;

public class ChristmasTrees extends Fragment {

    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach( context );
        this.context = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (context == null) context = getActivity();
    }

    private Integer[] trees={ R.drawable.ta,R.drawable.tb,R.drawable.tc,R.drawable.td,R.drawable.te,
            R.drawable.tf,R.drawable.tg,R.drawable.th,R.drawable.ti,R.drawable.tj,
            R.drawable.tk,R.drawable.tl,R.drawable.tm,R.drawable.tn,R.drawable.to,
            R.drawable.tq,R.drawable.tr,R.drawable.ts,R.drawable.tt,R.drawable.tp,
            R.drawable.tu,R.drawable.tv,R.drawable.tw,R.drawable.tx,R.drawable.ty,
            R.drawable.tz,R.drawable.taa,R.drawable.tab};
    GridView gridView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate( R.layout.christmas_stickers, null );
        gridView = (GridView) rootView.findViewById( R.id.gridview );
        gridView.setAdapter( new StickersAdapter( getContext(), trees ) );
        gridView.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                ((SelectIcon) context).getSticker( trees[position] );

            }
        } );
        return rootView;
    }
}
