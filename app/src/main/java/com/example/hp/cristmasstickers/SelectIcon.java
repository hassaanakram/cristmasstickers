package com.example.hp.cristmasstickers;

import android.content.Intent;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.hp.cristmasstickers.adapters.FragmentAdapter;


public class SelectIcon extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_select_icon );
        Toolbar toolbar = findViewById(R.id.layout_title_bar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null) {
            getSupportActionBar().setTitle("Click to Select Frame");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_circle_left);
        }

        TabLayout tabLayout = findViewById( R.id.tabs );

        ViewPager viewPager = findViewById( R.id.viewpager );
        FragmentAdapter adapter = new FragmentAdapter( getSupportFragmentManager() );
        viewPager.setOffscreenPageLimit( 2 );
        viewPager.setAdapter(adapter );
        tabLayout.setupWithViewPager( viewPager );

    }

    public void getSticker(Integer image) {
        int resID = getResources().getIdentifier( String.valueOf( image ), "id", getPackageName());
        Intent intent = new Intent();
        intent.putExtra( "fromIcon", true );
        intent.putExtra( "resource", resID );
        setResult( RESULT_OK, intent );
        finish();

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
