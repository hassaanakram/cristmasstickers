package com.example.hp.cristmasstickers.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.hp.cristmasstickers.R;
import com.example.hp.cristmasstickers.interfaces.cellbacks;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    Integer[] effects;
    Context context;
    cellbacks call;

    public RecyclerViewAdapter(Integer[] effects, Context context, cellbacks call) {
        this.effects = effects;
        this.context = context;
        this.call = call;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.from( viewGroup.getContext() )
                .inflate( R.layout.recyccler_view, viewGroup, false );

        return new MyViewHolder( itemView );
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i) {

        Glide.with( context ).load( effects[i] ).into( myViewHolder.effectsIv );

        myViewHolder.effectsIv.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call.CallBack( i );
            }
        } );
    }

    @Override
    public int getItemCount() {
        return effects.length;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView effectsIv;

        MyViewHolder(View view) {
            super( view );


            effectsIv = view.findViewById( R.id.recycler_iv );
        }
    }
}
