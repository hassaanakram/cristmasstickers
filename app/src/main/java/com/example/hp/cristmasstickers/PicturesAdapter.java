package com.example.hp.cristmasstickers;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class PicturesAdapter extends BaseAdapter {

    private Context ctx;
    private final String[] filesNames;
    private final String[] filesPaths;

    public PicturesAdapter(Context ctx, String[] filesNames, String[] filesPaths) {
        this.ctx = ctx;
        this.filesNames = filesNames;
        this.filesPaths = filesPaths;
    }
    @Override
    public int getCount() {
        return filesPaths.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ImageView imageView;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(ctx);
            imageView.setLayoutParams(new AbsListView.LayoutParams(285, 285));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 18, 8);
            Bitmap bmp = BitmapFactory.decodeFile(filesPaths[position]);
            if(bmp!=null){
            imageView.setImageBitmap(bmp);
            }
        } else {
            imageView = (ImageView) convertView;
        }

        return imageView;
    }
}
