package com.example.hp.cristmasstickers.ui;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.example.hp.cristmasstickers.BuildConfig;
import com.example.hp.cristmasstickers.R;
import com.example.hp.cristmasstickers.utils.Utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ja.burhanrashid52.photoeditor.PhotoEditor;
import ja.burhanrashid52.photoeditor.PhotoEditorView;
import okhttp3.internal.Util;

import static com.bumptech.glide.load.resource.bitmap.TransformationUtils.rotateImage;
import static com.example.hp.cristmasstickers.ui.BackgroundFramesActivity.REQUEST_TAKE_PHOTO;
import static com.example.hp.cristmasstickers.ui.DisplayActivity.PERMISSION_REQUEST_CODE;
import static com.example.hp.cristmasstickers.ui.MainActivity.PICK_FROM_GALLERY;

public class OverlayActivity extends AppCompatActivity implements View.OnClickListener {
    private SubsamplingScaleImageView backgroundImage;
    ImageView frameImage;
    ImageView cameraImage;
    ImageView galleryImage;
    PhotoEditorView photoEditorView;
    PhotoEditor mPhotoEditor;
    Bitmap largeIcon;
    Bitmap imageFromCameraBitmap, imageFromGalleryBitmp, frameFromActivityBitmap;
    FloatingActionButton floatingActionButton;
    View rlImages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overlay);

        backgroundImage = findViewById(R.id.background_image);
        frameImage = findViewById(R.id.frame_image);
        rlImages = findViewById(R.id.rlImages);

        initView();
        checkIntent();


    }

    private void initView() {
        floatingActionButton = findViewById(R.id.save_to_directory_overlay);
        floatingActionButton.setOnClickListener(this);
        cameraImage = findViewById(R.id.camera_image_overlay);
        cameraImage.setOnClickListener(this);
        galleryImage = findViewById(R.id.gallery_image_overlay);
        galleryImage.setOnClickListener(this);
        photoEditorView = findViewById(R.id.photoEditorViewOverlay);

        final Typeface mTextRobotoTf = ResourcesCompat.getFont(this, R.font.myfile);

        mPhotoEditor = new PhotoEditor.Builder(this, photoEditorView)
                .setPinchTextScalable(true)
                .setDefaultTextTypeface(mTextRobotoTf)
                .build();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("Discard")
                .setMessage("Do you want to discard the changes and go back?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        OverlayActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(R.mipmap.icon)
                .show();
    }


    private void checkIntent() {
        final Intent intent = getIntent();
        if (getIntent().hasExtra("id")) {
            int i = intent.getIntExtra("id", 1);

            switch (i) {
                case 0:
                    largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.frametoshowa);
                    frameImage.setImageBitmap(largeIcon);
                    frameFromActivityBitmap = largeIcon;

                    break;
                case 1:
                    largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.frametoshowb);
                    frameImage.setImageBitmap(largeIcon);
                    break;
                case 2:

                    largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.frametoshowc);
                    frameImage.setImageBitmap(largeIcon);
                    frameFromActivityBitmap = largeIcon;

                    break;

                case 4:
                    largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.frametoshowe);
                    frameImage.setImageBitmap(largeIcon);
                    frameFromActivityBitmap = largeIcon;

                    break;

                case 6:
                    largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.frametoshowg);
                    frameImage.setImageBitmap(largeIcon);
                    frameFromActivityBitmap = largeIcon;

                    break;
                case 7:
                    largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.frametoshowh);
                    frameImage.setImageBitmap(largeIcon);
                    frameFromActivityBitmap = largeIcon;

                    break;
                case 8:
                    largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.frametoshowi);
                    frameImage.setImageBitmap(largeIcon);
                    frameFromActivityBitmap = largeIcon;

                    break;
                case 9:
                    largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.frametoshowx);
                    frameImage.setImageBitmap(largeIcon);
                    frameFromActivityBitmap = largeIcon;

                    break;
                case 10:
                    largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.frametoshowy);
                    frameImage.setImageBitmap(largeIcon);
                    frameFromActivityBitmap = largeIcon;

                    break;
                case 11:
                    largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.frametoshowz);
                    frameImage.setImageBitmap(largeIcon);
                    frameFromActivityBitmap = largeIcon;
                    break;


            }


        }

    }

    private boolean cameraCheckPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    private void cameraRequestPermission() {

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA},
                PERMISSION_REQUEST_CODE);
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(this,
                        "com.example.hp.cristmasstickers",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                //       photoURI=null;
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);

            }
        }
    }

    private String mCurrentPhotoPath = "", frameFilePath, filePath;
    Uri photoURI;

    private String createImagePath() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        filePath = image.getAbsolutePath();
        return filePath;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        String root = getExternalFilesDir(Environment.DIRECTORY_DCIM).getAbsolutePath() + "/Christmas Wallpapers";
        File file = new File(root);
        if (!file.exists())
            file.mkdirs();
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                file      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save_to_directory_overlay:

//                if (imageFromGalleryBitmp != null) {
//                    overlay(imageFromGalleryBitmp, frameFromActivityBitmap );
//
//                } else {
//                    overlay(imageFromCameraBitmap, frameFromActivityBitmap);
//                }


                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(OverlayActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(OverlayActivity.this);
                }

                builder.setTitle("Save Image")
                        .setMessage("Are you sure you want to save this Image?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with save
                                if (ActivityCompat.checkSelfPermission(OverlayActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                                    ActivityCompat.requestPermissions(OverlayActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                                    return;
                                }
                                try {
                                    filePath = createImagePath();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                Bitmap bitmap = loadBitmapFromView(rlImages);
                                saveBitmapToFile(filePath, bitmap);

                                Intent intent = new Intent(OverlayActivity.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();

                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(R.drawable.icon)
                        .show();

                break;
            case R.id.camera_image_overlay:
                if (cameraCheckPermission()) {
                    dispatchTakePictureIntent();

                } else {
                    cameraRequestPermission();
                }
                break;
            case R.id.gallery_image_overlay:
                if (galleryCheckPermission()) {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
                } else {
                    galleryRequestPermission();
                }
                break;
        }
    }

    private boolean galleryCheckPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            return false;

        }
        return true;

    }

    private void galleryRequestPermission() {
        ActivityCompat.requestPermissions(this, new String[]
                        {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                PICK_FROM_GALLERY);

    }

    public void overlay(Bitmap bmp1, Bitmap bmp2) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp2.getWidth(), bmp2.getHeight(), bmp2.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, new Matrix(), null);
        canvas.drawBitmap(bmp2, 0, 0, null);
        photoEditorView.getSource().setImageBitmap(bmOverlay);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            ExifInterface ei = null;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            if (requestCode == REQUEST_TAKE_PHOTO) {


                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), photoURI);


                    ei = new ExifInterface(mCurrentPhotoPath);

                } catch (IOException e) {
                    e.printStackTrace();
                }

                Bitmap rotatedBitmap = null;
                assert ei != null;
                int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED);
                switch (orientation) {

                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotatedBitmap = rotateImage(bitmap, 90);
                        break;


                    case ExifInterface.ORIENTATION_ROTATE_180:

                        rotatedBitmap = rotateImage(bitmap, 90);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotatedBitmap = rotateImage(bitmap, 270);
                        break;

                    case ExifInterface.ORIENTATION_NORMAL:
                    default:
                        rotatedBitmap = bitmap;
                }
                backgroundImage.setImage(ImageSource.bitmap(rotatedBitmap));
                imageFromCameraBitmap = rotatedBitmap;
            } else if (requestCode == PICK_FROM_GALLERY && data != null) {
                final Uri galleryImageUri = data.getData();

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), galleryImageUri);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                    byte[] byteArray = stream.toByteArray();
                    Bitmap compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

                    backgroundImage.setImage(ImageSource.bitmap(compressedBitmap));

                    imageFromGalleryBitmp = compressedBitmap;

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Bitmap loadBitmapFromView(View v) {
        Bitmap b = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);
        return b;
    }

    public void saveBitmapToFile(String filePath, Bitmap bmp) {

        try {
            FileOutputStream out = new FileOutputStream(filePath);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
