package com.example.hp.cristmasstickers.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;

import android.support.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;

import android.support.v4.content.res.ResourcesCompat;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.example.hp.cristmasstickers.BuildConfig;
import com.example.hp.cristmasstickers.R;
import com.example.hp.cristmasstickers.SelectIcon;
import com.example.hp.cristmasstickers.adapters.RecyclerViewAdapter;
import com.example.hp.cristmasstickers.interfaces.cellbacks;
import com.example.hp.cristmasstickers.utils.AdUtils;
import com.example.hp.cristmasstickers.utils.Utils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;


import ja.burhanrashid52.photoeditor.PhotoEditor;
import ja.burhanrashid52.photoeditor.PhotoEditorView;
import ja.burhanrashid52.photoeditor.PhotoFilter;
import yuku.ambilwarna.AmbilWarnaDialog;


import static com.bumptech.glide.load.resource.bitmap.TransformationUtils.rotateImage;

public class DisplayActivity extends AppCompatActivity implements cellbacks {
    PhotoEditor mPhotoEditor;
    ImageView effectsIv, createText, emoji, sticker;
    private String filePath;
    int counter = 0;
    Bitmap icon;
    InterstitialAd interstitialAd;
    PhotoEditorView photoEditorView;
    public static final int PERMISSION_REQUEST_CODE = 1;
    RecyclerView effectsRecyclerView;
    FloatingActionButton fab;
    private File picImage;
    LinearLayout footerLayout;
    private Integer[] efeects = {R.drawable.autofixx, R.drawable.bnw, R.drawable.brightnesss, R.drawable.contrasts,
            R.drawable.crossprocessing, R.drawable.duotonee, R.drawable.filllight,
            R.drawable.film_doc, R.drawable.fisheye, R.drawable.fliphorizontal,
            R.drawable.flipvertical, R.drawable.grains, R.drawable.grayscale, R.drawable.luminense,
            R.drawable.negatives, R.drawable.posterixe, R.drawable.rotates,
            R.drawable.saturations, R.drawable.sepia, R.drawable.sharpnesss,
            R.drawable.temprature, R.drawable.tint, R.drawable.viginitee};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        Toolbar toolbar = findViewById(R.id.layout_title_bar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Click to Select Frame");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_circle_left);
        }

        effectsRecyclerView = findViewById(R.id.effects_rv);
        LinearLayoutManager layoutManager = new LinearLayoutManager(DisplayActivity.this, LinearLayoutManager.HORIZONTAL, false);
        effectsRecyclerView.setLayoutManager(layoutManager);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(efeects, DisplayActivity.this, DisplayActivity.this);
        effectsRecyclerView.setAdapter(adapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(effectsRecyclerView.getContext(),
                layoutManager.getOrientation());
        effectsRecyclerView.addItemDecoration(dividerItemDecoration);

        emoji = findViewById(R.id.add_emoji);
        fab = findViewById(R.id.save_to_directory);
        effectsIv = findViewById(R.id.add_effects);
        footerLayout = findViewById(R.id.footer);
        createText = findViewById(R.id.create_text);
        sticker = findViewById(R.id.add_sticker);
        photoEditorView = findViewById(R.id.photoEditorView);

        final Typeface mTextRobotoTf = ResourcesCompat.getFont(this, R.font.myfile);

        mPhotoEditor = new PhotoEditor.Builder(this, photoEditorView)
                .setPinchTextScalable(true)
                .setDefaultTextTypeface(mTextRobotoTf)
                .build();

        interstitialAd = new InterstitialAd(this);
        AdUtils.requestNewInterstitial(this, interstitialAd);


        String mCurrentPhotoPath;
        final Intent intent = getIntent();
        filePath = intent.getStringExtra("CameraImage");
        if (getIntent().hasExtra("CameraImage")) {
            ExifInterface ei = null;
            mCurrentPhotoPath = intent.getStringExtra("CameraImage");
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, options);

            try {
                ei = new ExifInterface(mCurrentPhotoPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap rotatedBitmap = null;
            if (ei != null) {
                int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED);


                switch (orientation) {

                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotatedBitmap = rotateImage(bitmap, 90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:

                        rotatedBitmap = rotateImage(bitmap, 90);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotatedBitmap = rotateImage(bitmap, 270);
                        break;

                    case ExifInterface.ORIENTATION_NORMAL:
                    default:
                        rotatedBitmap = bitmap;
                }
            } else {
                Toast.makeText(this, "Kindly take photo again ", Toast.LENGTH_SHORT).show();
            }
            if (rotatedBitmap != null) {
                Bitmap bitmmapResized = getResizedBitmap(rotatedBitmap, rotatedBitmap.getWidth() / 2, rotatedBitmap.getHeight() / 2);
                photoEditorView.getSource().setImageBitmap(bitmmapResized);
            }


        } else if (getIntent().hasExtra("BitmapImageGallery")) {
            Uri imageUri = Uri.parse(intent.getStringExtra("BitmapImageGallery"));

            photoEditorView.getSource().setImageURI(imageUri);

        }

        sticker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentst = new Intent(DisplayActivity.this, SelectIcon.class);
                intentst.putExtra("fromDisplay", true);
                intentst.putExtra("fromStickers", false);
                AdUtils.showAdAndMoveForwardForResult(DisplayActivity.this, interstitialAd, intentst, 111);
            }
        });
        effectsIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerLayout.setVisibility(View.GONE);
                effectsRecyclerView.setVisibility(View.VISIBLE);
            }
        });

        emoji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentEmoji = new Intent(DisplayActivity.this, EmojiActivity.class);
                AdUtils.showAdAndMoveForwardForResult(DisplayActivity.this, interstitialAd, intentEmoji, 333);
                interstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showAd();

            }
        });

        createText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DisplayActivity.this);
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

    }

    private void showAd() {
        if (interstitialAd.isLoaded()) {
            interstitialAd.show();
            interstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    AdUtils.requestNewInterstitial(DisplayActivity.this, interstitialAd);
                    OnClickAfterAd();
                }

                @Override
                public void onAdClicked() {
                    super.onAdClicked();
                    AdUtils.requestNewInterstitial(DisplayActivity.this, interstitialAd);
                    OnClickAfterAd();
                }
            });
        } else {
            OnClickAfterAd();
        }
    }

    private void showAdWithoutforward() {
        if (interstitialAd.isLoaded()) {
            interstitialAd.show();
            interstitialAd.loadAd(new AdRequest.Builder().build());
            interstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    AdUtils.requestNewInterstitial(DisplayActivity.this, interstitialAd);
                }

                @Override
                public void onAdClicked() {
                    super.onAdClicked();
                }
            });
        } else {
            interstitialAd.loadAd(new AdRequest.Builder().build());
        }
    }

    EditText getText;
    String text;
    Button send, pickColor;
    String colorToSend;
    ImageView dismisss;

    public void showDialog(final Activity activity) {

        final Dialog dialog = new Dialog(activity);
        @SuppressLint("InflateParams") View view = activity.getLayoutInflater().inflate(R.layout.text_dialog, null);

        dialog.setContentView(view);
        getText = dialog.findViewById(R.id.get_text_dialog);

//        Window window = dialog.getWindow();
//        window.setLayout( LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT );
        text = getText.getText().toString();
        send = dialog.findViewById(R.id.send_dialog);
        pickColor = dialog.findViewById(R.id.pick_color_dialog);
        dismisss = dialog.findViewById(R.id.dismisss);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNotEmpty(getText.getText())) {
                    text = getText.getText().toString();
                    if (Utils.isNotEmpty(colorToSend)) {
                        mPhotoEditor.addText(text, Integer.parseInt(colorToSend));
                        dialog.dismiss();
                    } else {
                        mPhotoEditor.addText(text, -16777216);
                        dialog.dismiss();
                    }
                } else {
                    Toast.makeText(activity, "Please enter text", Toast.LENGTH_SHORT).show();
                }
                showAdWithoutforward();

            }
        });
        pickColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AmbilWarnaDialog dialog = new AmbilWarnaDialog(DisplayActivity.this, Color.parseColor("#000000"),
                        new AmbilWarnaDialog.OnAmbilWarnaListener() {
                            @Override
                            public void onCancel(AmbilWarnaDialog dialog) {

                            }

                            @Override
                            public void onOk(AmbilWarnaDialog dialog, int color) {
                                if (text != null) {
                                    colorToSend = String.valueOf(color);
                                    getText.setTextColor(color);
                                }

                            }
                        });
                dialog.show();
            }
        });
        dismisss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private String createImagePath() throws IOException {
        // Create an image file name
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File pictureDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + "/Christmas/");
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        if (!pictureDirectory.exists()) {
            pictureDirectory.mkdirs();
        }
        picImage = File.createTempFile(
                imageFileName,
                ".jpg",
                pictureDirectory
        );

        // Save a file: path for use with ACTION_VIEW intents
        filePath = image.getAbsolutePath();
        return filePath;
    }

    @Override
    public void onBackPressed() {

        if (effectsRecyclerView.isShown()) {
            footerLayout.setVisibility(View.VISIBLE);
            effectsRecyclerView.setVisibility(View.GONE);
        } else {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(this);
            }
            builder.setTitle("Discard")
                    .setMessage("Do you want to discard the changes and go back?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            DisplayActivity.super.onBackPressed();
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(R.mipmap.icon)
                    .show();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(DisplayActivity.this, "Permission: " + permissions[0] + "was " + grantResults[0], Toast.LENGTH_SHORT).show();
        }
    }

    // download to ic_save check late
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 111) {
                assert data != null;
                int resID = data.getIntExtra("resource", R.drawable.download);
                icon = BitmapFactory.decodeResource(getResources(),
                        resID);
                mPhotoEditor.addImage(icon);


            } else if (requestCode == 333) {

                if (data != null) {
                    String emojiId = data.getStringExtra("emoji");
                    mPhotoEditor.addEmoji(emojiId);

                }
            } else if (requestCode == 444) {

                assert data != null;
                String urlToLoad = data.getStringExtra("frameToShow");
                if (urlToLoad != null) {
                    icon = BitmapFactory.decodeFile(urlToLoad);
                    mPhotoEditor.addImage(icon);
                    //
                    // Glide.with( this ).load( urlToLoad ).into(  frame);
                }
            } else {
                if (data != null) {
                    String resID = data.getStringExtra("text");
                    String colorToShow = data.getStringExtra("color");
                    mPhotoEditor.addText(resID, Integer.parseInt(colorToShow));

                } else {
                    mPhotoEditor.addText("Nothing Entered", 123);

                }


            }
        }
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void CallBack(int i) {
        switch (i) {
            case 0:
                mPhotoEditor.setFilterEffect(PhotoFilter.AUTO_FIX);
                break;
            case 1:
                mPhotoEditor.setFilterEffect(PhotoFilter.BLACK_WHITE);
                break;
            case 2:
                mPhotoEditor.setFilterEffect(PhotoFilter.BRIGHTNESS);
                break;
            case 3:
                mPhotoEditor.setFilterEffect(PhotoFilter.CONTRAST);
                break;
            case 4:
                mPhotoEditor.setFilterEffect(PhotoFilter.CROSS_PROCESS);
                break;
            case 5:
                mPhotoEditor.setFilterEffect(PhotoFilter.DUE_TONE);
                break;
            case 6:
                mPhotoEditor.setFilterEffect(PhotoFilter.FILL_LIGHT);
                break;
            case 7:
                mPhotoEditor.setFilterEffect(PhotoFilter.DOCUMENTARY);
                break;
            case 8:
                mPhotoEditor.setFilterEffect(PhotoFilter.FISH_EYE);
                break;
            case 9:
                mPhotoEditor.setFilterEffect(PhotoFilter.FLIP_HORIZONTAL);
                break;
            case 10:
                mPhotoEditor.setFilterEffect(PhotoFilter.FLIP_VERTICAL);
                break;
            case 11:
                mPhotoEditor.setFilterEffect(PhotoFilter.GRAIN);
                break;
            case 12:
                mPhotoEditor.setFilterEffect(PhotoFilter.GRAY_SCALE);
                break;
            case 13:
                mPhotoEditor.setFilterEffect(PhotoFilter.LOMISH);
                break;
            case 14:
                mPhotoEditor.setFilterEffect(PhotoFilter.NEGATIVE);
                break;
            case 15:
                mPhotoEditor.setFilterEffect(PhotoFilter.POSTERIZE);
                break;
            case 16:
                mPhotoEditor.setFilterEffect(PhotoFilter.ROTATE);
                break;
            case 17:
                mPhotoEditor.setFilterEffect(PhotoFilter.SATURATE);
                break;
            case 18:
                mPhotoEditor.setFilterEffect(PhotoFilter.SEPIA);
                break;
            case 19:
                mPhotoEditor.setFilterEffect(PhotoFilter.SHARPEN);
                break;
            case 20:
                mPhotoEditor.setFilterEffect(PhotoFilter.TEMPERATURE);
                break;
            case 21:
                mPhotoEditor.setFilterEffect(PhotoFilter.TINT);
                break;
            case 22:
                mPhotoEditor.setFilterEffect(PhotoFilter.VIGNETTE);
                break;
        }
        showAdWithoutforward();
    }

    private void OnClickAfterAd() {
        counter++;
        if (counter % 4 == 0 && interstitialAd.isLoaded()) {
            showAd();
        }
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(DisplayActivity.this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(DisplayActivity.this);
        }

        builder.setTitle("Save Image")
                .setMessage("Are you sure you want to save this Image?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with save
                        if (ActivityCompat.checkSelfPermission(DisplayActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                            ActivityCompat.requestPermissions(DisplayActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                            return;
                        }
                        try {
                            filePath = createImagePath();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        mPhotoEditor.saveAsFile(filePath, new PhotoEditor.OnSaveListener() {
                            @Override
                            public void onSuccess(@NonNull String imagePath) {
                                Toast.makeText(DisplayActivity.this, "saved", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(DisplayActivity.this, MainActivity.class);
                                startActivity(intent);
                                try {
                                    copy(new File(filePath), picImage);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                finish();
                            }

                            @Override
                            public void onFailure(@NonNull Exception exception) {

                            }
                        });


                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(R.drawable.icon)
                .show();
    }

    public void copy(File src, File dst) throws IOException {
        if (android.os.Build.VERSION.SDK_INT > 18) {
            try (InputStream in = new FileInputStream(src)) {
                try (OutputStream out = new FileOutputStream(dst)) {
                    // Transfer bytes from in to out
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                }
            }
        } else {
            InputStream in = new FileInputStream(src);
            try {
                OutputStream out = new FileOutputStream(dst);
                try {
                    // Transfer bytes from in to out
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                } finally {
                    out.close();
                }
            } finally {
                in.close();
            }
        }
    }
}
